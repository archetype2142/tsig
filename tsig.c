#include <unistd.h>
#include <signal.h>
#include <stdio.h>
#include <pthread.h>

#define NUM_CHILD 10
#define WITH_SIGNALS


#ifdef WITH_SIGNALS
int interrupted;
void interrupt(int);
int interrupt_child(int pid);
#endif

int main() {
	int i, j, status;
	int pids[NUM_CHILD];

#ifdef WITH_SIGNALS
	sigset_t set;

	sigfillset(&set);
	sigdelset(&set, SIGCHLD);
	sigdelset(&set, SIGINT);
	sigprocmask(SIG_BLOCK, &set, NULL);
	interrupted = 0;
	signal (SIGINT, interrupt);
#endif

	for (i = 0; i < NUM_CHILD; i++) {

#ifdef WITH_SIGNALS
		if (interrupted) {
			printf ("parent[%d]: sending SIGTERM signal\n", getppid());
			for (j = 0; j < i; j++) 
				kill(pids[j], SIGTERM);
			printf ("child[%d]: received SIGTERM signal, terminating\n", getpid());
								
			break;
		}
#endif
		printf ("parent[%d]: creating child %d\n", getpid(),i);
		pids[i] = fork();

		signal (SIGTERM, interrupt_child(getpid()));
		
		switch (pids[i]) {
			case -1:
				printf ("error!\n");
				for (j = 0; j < i; j++) 
					kill(pids[j], SIGTERM);
				exit(1);
			break;

			case 0: 
				printf ("child[%d]: Parent: %d\n", getpid(), getppid());
				sleep(10);
				printf ("child[%d]: complete\n", getpid());
				exit(0);
			break;

			default: 
			break;
		}
		sleep(1);
	}
	
	status = 0;
	i = 0;

	do {
		status = wait(NULL);
		if (status != -1) i++;
	} while (status != -1);
	printf ("parent[%d]: %i children terminating.\nparent[%d]: There are no more children\n", getpid(), i, getpid());

#ifdef WITH_SIGNALS
	if (interrupted) 
		return 1;
#endif
	return 0;
}
int interrupt_child(int pid) {
	printf ("child[%d]: interrupt detected\n", pid);
}

void interrupt(int signal) {
	interrupted = 1;
	printf ("Interrupted!\n");
}
